from controllers.studentController import *
from controllers.teacherController import *
from controllers.tgController import *
from controllers.groupsController import *
from app import *


if __name__ == '__main__':
    app.run()
