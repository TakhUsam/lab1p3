from flask import Flask, jsonify, abort, request
from models.teacher_group import teacher_group
from app import *


tg={
    1:teacher_group(1, 1, 1),
    2:teacher_group(2, 1, 2),
    3:teacher_group(3, 1, 3),
    4:teacher_group(4, 2, 1),
    5:teacher_group(5, 2, 2),
}


@app.route('/teach_groups/', methods=['GET'])
def get_tgs():
    return jsonify([Student.to_json() for Student in tg.values()])


@app.route('/teach_groups/<int:stud_id>', methods=['GET'])
def get_tg(stud_id):
    if stud_id in tg:
        return jsonify(tg[stud_id].to_json())
    abort(404)


@app.route('/teach_groups/', methods=['POST'])
def post_tg():
    dat = request.form
    new_id = len(tg) + 1
    student = teacher_group(new_id, dat['id_teach'], dat['id_group'])
    tg[new_id]=student
    return jsonify(student.to_json())


@app.route('/teach_groups/<tg_id>', methods=['PUT'])
def update_tg(tg_id):
    dat = request.form
    r = None
    if not tg_id in tg:
        abort(404)
    student = teacher_group(tg_id, dat['id_teach'], dat['id_group'])
    tg[tg_id]=student
    return jsonify(student.to_json())


@app.route('/teach_groups/<tg_id>', methods=['DELETE'])
def delete_tg(tg_id):
    if not tg_id in tg:
        abort(404)
    else:
        st=tg[tg_id]
        del tg[tg_id]
    return st.to_json()

