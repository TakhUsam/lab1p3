from flask import Flask, jsonify, abort, request
from models.student import Student
from controllers.tgController import tg
from app import *



stud={
    1:Student(1, 'Takhaev Usam', 19, 1),
    2:Student(2, 'Arsaev Umar', 23, 1),
    3:Student(3, 'Bairakov Hamza', 19, 1),
    4:Student(4, 'Murdalov Black Nigga', 20, 2),
    5:Student(5, 'Shovhalov Razamber', 19, 2),
    6:Student(7, 'Margoshvili Aslan', 24, 3)
}

@app.route('/', methods=['GET'])
def mai():
    return jsonify([Student.to_json() for Student in stud.values() ])


@app.route('/students/', methods=['GET'])
def get_students():
    return jsonify([Student.to_json() for Student in stud.values()])


@app.route('/students/<int:stud_id>', methods=['GET'])
def get_student(stud_id):
    if stud_id in stud:
        return jsonify(stud[stud_id].to_json())
    abort(404)


@app.route('/students/', methods=['POST'])
def post_students():
    dat = request.form
    new_id = len(stud) + 1
    student = Student(new_id, dat['FIO'], dat['Age'])
    stud[new_id]=student
    return jsonify(student.to_json())


@app.route('/students/<int:stud_id>', methods=['PUT'])
def update_students(stud_id):
    dat = request.form
    r = None
    if not stud_id in stud:
        abort(404)
    student = Student(stud_id, dat['FIO'], dat['Age'])
    stud[stud_id]=student
    return jsonify(student.to_json())


@app.route('/students/<int:stud_id>', methods=['DELETE'])
def delete_students(stud_id):
    if not stud_id in stud:
        abort(404)
    else:
        st=stud[stud_id]
        del stud[stud_id]
    return st.to_json()


@app.route('/students/<int:stud_id>/teachers', methods=['GET'])
def get_teachs(stud_id):
    if not stud_id in stud:
        abort(404)
    st=stud[stud_id]
    id_group=st.id_group
    return jsonify([t.id_teach for t in tg.values() if t.id_group==id_group])