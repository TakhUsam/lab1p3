from flask import Flask, jsonify, abort, request
from controllers.tgController import tg

from models.teacher import Teacher
from app import *



teachers={
    1:Teacher(1, 'Гериханов Зелим', 27),
    2:Teacher(2, 'Хотов Азамат', 33),
    3:Teacher(3, 'Джангаров Ахьмад', 24),
    4:Teacher(4, 'Магомедов Ислам', 27)
}


@app.route('/teachers/', methods=['GET'])
def get_teachers():
    return jsonify([Teacher.to_json() for Teacher in teachers.values()])


@app.route('/teachers/<int:teach_id>', methods=['GET'])
def get_teacher(teach_id):
    if teach_id in teachers:
        return jsonify(teachers[teach_id].to_json())
    abort(404)


@app.route('/teachers/', methods=['POST'])
def post_teachers():
    dat = request.form
    new_id = len(teachers) + 1
    teacher = Teacher(new_id, dat['FIO'], dat['Age'])
    teachers[new_id]=teacher
    return jsonify(teacher.to_json())


@app.route('/teachers/<int:teach_id>', methods=['PUT'])
def update_teachers(teach_id):
    dat = request.form
    r = None
    if not teach_id in teachers:
        abort(404)
    teacher = Teacher(teach_id, dat['FIO'], dat['Age'])
    teachers[teach_id]=teacher
    return jsonify(teacher.to_json())


@app.route('/teachers/<int:teach_id>', methods=['DELETE'])
def delete_teachers(teach_id):
    if not teach_id in teachers:
        abort(404)
    else:
        teacher=teachers[teach_id]
        del teachers[teach_id]
    return teacher.to_json()


@app.route('/teachers/<int:teach_id>/groups', methods=['GET'])
def get_groups(teach_id):
    if not teach_id in teachers:
        abort(404)
    groups=[g.id_group for g in tg.values() if g.id_teach==teach_id]
    return jsonify(groups)