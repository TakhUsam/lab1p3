from flask import Flask, jsonify, abort, request
from models.group import Group
from app import *
from controllers.tgController import tg
from controllers.studentController import stud



groups={
   1:Group(1, 'PI', 2017),
   2:Group(2, 'BIn', 2018),
   3:Group(3, 'ITIS', 2018),
   4:Group(4, 'PIII', 2020),
}

@app.route('/groups/', methods=['GET'])
def get_groupes():
    return jsonify([Teacher.to_json() for Teacher in groups.values()])


@app.route('/groups/<int:group_id>', methods=['GET'])
def get_group(group_id):
    if group_id in groups:
        return jsonify(groups[group_id].to_json())
    abort(404)


@app.route('/groups/', methods=['POST'])
def post_courses():
    dat = request.form
    new_id = len(groups) + 1
    course = Group(new_id, dat['name'], dat['year'])
    groups[new_id]=course
    return jsonify(course.to_json())


@app.route('/groups/<int:group_id>', methods=['PUT'])
def update_group(group_id):
    dat = request.form
    r = None
    if not group_id in groups:
        abort(404)
    course = Group(group_id, dat['name'], dat['year'])
    groups[group_id]=course
    return jsonify(course.to_json())


@app.route('/groups/<int:group_id>', methods=['DELETE'])
def delete_group(group_id):
    if not group_id in groups:
        abort(404)
    else:
        st=groups[group_id]
        del groups[group_id]
    return st.to_json()


@app.route('/groups/<int:group_id>/teachers', methods=['GET'])
def get_group_teachers(group_id):
    if not group_id in groups:
        abort(404)
    return [_tg.id_teach for _tg in tg.values() if _tg.id_group==group_id]


@app.route('/groups/<group_id>/students', methods=['GET'])
def get_group_students(group_id):
    if not group_id in groups:
        abort(404)
    return [st for st in stud.values() if st.id_group==group_id]